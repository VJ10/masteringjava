class Anagram{

    public static boolean containLetter(char letter, String wrd){
        boolean res = false;
        
        for(int i = 0; i<wrd.length(); i++){
            if(letter == wrd.charAt(i)){
                res  =  true ;
                break;
            }
        }

        return res;
    }



    public static boolean isAnagram(String wd1, String wd2){
        boolean res = true;

        if(wd1.length() != wd2.length()){
            res = false;
        }

        else
        {

        
        for(int i = 0; i < wd1.length(); i++){
            
            if(  containLetter( Character.toLowerCase(wd1.charAt(i)), wd2.toLowerCase()) ){
                continue;
            }

            else{
                res = false;
            }
        }


        }
        System.out.println(wd1 + " = " + wd2 + " => " + res);
        return res;
    }



    public static void main(String[] args) {
        isAnagram("meteor", "remote");
        isAnagram("act", "cat");
        isAnagram("gainly", "laying");
        isAnagram("study", "dusty");
        isAnagram("bad credit", "debit card");
        isAnagram("older and wiser", "I learned words");        
    }
}