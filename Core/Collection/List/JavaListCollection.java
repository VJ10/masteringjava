import java.util.*;

class JavaListCollection{
    public static void main(String[] args) {
    List <Integer> aLst = new ArrayList<Integer>();
    aLst.add(54);
    aLst.add(13);
    aLst.add(23);

    Iterator itr = aLst.iterator();
    while(itr.hasNext()){
        System.out.println(itr.next());
    }
    List <Integer> b = new LinkedList<>();
    }
}

/**
 * Child of collection interface
 * List Interface in implemeted by
 *  ArrayList
 *  Linked List
 *  Vector
 *  Stack
 */