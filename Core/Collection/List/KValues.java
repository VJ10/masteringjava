import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class KValues
{
    private ArrayList <Integer> lst;

    public KValues(int k){
        lst = new ArrayList<>();
    }

    public void addItem(int item){
        lst.add(item);
    }

    public void addList(List <Integer> a){
        for(int item : a){
            lst.add(item);
        }
    }

    public void KValuesResult(int k){
        HashMap<Integer, Integer> kValueMap = new HashMap<>();
        for(int item : lst){
            kValueMap.put(item, item+k);
        }

        for(Integer key : kValueMap.keySet()){

            if(lst.contains(kValueMap.get(key))){
                System.out.println("(" + key  + ", " + kValueMap.get(key) + ")");
                // System.out.println("Key: " + key + ", Value: " + kValueMap.get(key));
            }
        }
    }

    public static void main(String[] args) {
        
        KValues arr = new KValues(2);

        arr.addItem(3);
        arr.addItem(9);
        arr.addItem(8);
        arr.addItem(2);
        arr.addItem(10);
        arr.addItem(13);
        arr.addItem(5);
        arr.addItem(15);
        arr.addItem(4);

        arr.addList(Arrays.asList(25,13,39,43, 27, 45, 57));
        arr.KValuesResult(15);
        
    }
}
