abstract class Beverage{
    public String description = "Unknown Beverage";
    
    public String getDescription(){
        return description;
    }


    public abstract double cost();
    
}

/**  COMPONENT : Interface for object that can have responsabilities added dynamically
 * We are making this abstract so we can make sure that all the propertis are there 
 * for each Coffe or tee. Subclasses will have to implement their own cost. This is the component class. 
 * To add new behavior to the component then we must create a new Decorator which need to have the same
 * subtype as the object that it decorates.
 */