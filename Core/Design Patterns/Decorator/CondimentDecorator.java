public abstract class CondimentDecorator extends Beverage{
    public abstract String getDescription();
}

/**
 * Add the Condiment Decorator must reimplement the getDescription() methods.
 * Adding Behavior to the Component. Use inheritance to ensure that they have
 * the same subtype. Extends the functionality of the component by adding state or adding behavior
 * Need to decorate woith condiments such as Whip, Milk, Soy and Mocha
 * */ 