public class Espresso extends Beverage{
    public Espresso(){
        description = "Espresso";
    }
    @Override
    public double cost() {
        return 1.99;
    }
    
}

/**    CONCRETE COMPONENT
 * This is a Beverage. The description is an instance variable of beverage.
 * No need to compute the cost of an Espresso, it comes with it.
 */