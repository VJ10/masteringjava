public class HouseBlend extends Beverage{
    public HouseBlend(){
        description = "House Blend Coffee";
    }

    public double cost(){
        return .89;
    }
}

/**
 * Concrete Component
 * Defines an object to which additional responsibilies can be added to
 */