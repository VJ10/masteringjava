class Milk extends CondimentDecorator{
    private Beverage beverage;

    public Milk(Beverage bev){
        this.beverage = bev;
    }

    public double cost(){
        return beverage.cost() + .75;
    }

    public String getDescription(){
        return beverage.getDescription() + " , Milk.";
    }
}