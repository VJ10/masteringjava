public class Mocha extends CondimentDecorator{
    Beverage beverage;
    
    public Mocha(Beverage bev){
        this.beverage = bev;
    }

    public String getDescription(){
        return beverage.getDescription() + ", Mocha";
    }

    public double cost(){
        return .20 + beverage.cost();
    }
}

/**
 * Condiment, concrete decorator
 */