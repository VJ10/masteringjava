public class Soy extends CondimentDecorator{
    private Beverage beverage;

    public Soy(Beverage bev){
        this.beverage = bev;
    }

    public String getDescription(){
        return beverage.getDescription() + " , Soy ";
    }

    public double cost(){
        return beverage.cost() + .50;
    }
}