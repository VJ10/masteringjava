class Starbuz{
    public static void main(String[] args) {
        Beverage beverage = new Espresso();
        System.out.println(beverage.getDescription() + " $" + beverage.cost());

        Beverage beverage2 = new HouseBlend();
        System.out.println(beverage2.getDescription() + " $" + beverage2.cost());

        // Wrapping Begins
        beverage2 = new Milk(beverage2);
        System.out.println("New Cost " + beverage2.getDescription() + " , $" + beverage2.cost());

        
    }
}