class Whip extends CondimentDecorator{
    private Beverage beverage;

    public Whip(Beverage bev){
        this.beverage = bev;
    }

    public String getDescription(){
        return beverage.getDescription() + " , Whip ";
    }    

    public double cost(){
        return beverage.cost() + 2.00;
    }
}