public class ChicagoPizzaStore extends PizzaStore{
    public ChicagoPizzaStore(){super();}

    public Pizza createPizza(String item){
        if(item.equals("cheese")){
            return new ChicagoStyleCheesePizza();
        }
        else return null;
    }
}