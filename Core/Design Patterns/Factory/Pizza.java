import java.util.ArrayList;

abstract class Pizza{
    public String name;
    public String dough;
    public String sauce;
    ArrayList <String>toppings = new ArrayList<>();

    public void prepare(){
        System.out.println("Preparing " + name);
        System.out.println("Tossing dough...");
        System.out.println("Adding sauce...");
        System.out.println("Adding toppings: ");

        for (String top : toppings) {
            System.out.println(" " + top);
        }

        // for (int i = 0; i < toppings.size(); i++) {
        // System.out.println(" " + toppings.get(i));
        // }
    }

    public void bake(){
        System.out.println("Pizza Is Currently Baking");
    }

    public void cut(){
        System.out.println("Cutting Pizza");
    }

    public void box(){
        System.out.println("Putting Pizza In Box");
    }

    public String getName(){
        return name;
    }
}

/**
 * This is the product of the factory, pizza.
 * We have defined Pizza as an abstract class with soe helpful implemetation
 * that can be overridden.
 */