public abstract class PizzaStore{
    SimplePizzaFactory factory;

    public PizzaStore(){
    
    }

    protected abstract Pizza createPizza(String type); // All the instantiation goes here, acts as a factory. Must be handled by the subclass

    public Pizza orderPizza(String type){
        Pizza pizza;
        pizza = createPizza(type);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}

/**
 * also a client of the factory. By encapsulating the pizza creating in one class, we know that there is only one place to make
 * modifications when the impletation cahnges. We are just able to remove the concreate instantiation from client code.
 * It must go through the SimplePizzaFactory to get instances of Pia.
 * 
 * 
 * 
 * Subclasses of PizzaStore handle object instantiation for us in the createPizza() method.
 * Can serve many different factories. SimplePizza, NYPizzaFactory, ChicagoPizza
 */