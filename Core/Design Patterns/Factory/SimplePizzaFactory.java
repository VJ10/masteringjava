public class SimplePizzaFactory{
    // this method is used so all client can initiate new objects. only job is to create pizza for its clients
    public Pizza createPizza(String type){
        Pizza pizza = null;

        if(type.equals("cheese")){
            pizza = new CheesePizza();
        }else if(type.equals("pepperoni")){
            pizza = new PepperoniPizza();
        }

    return pizza;
    }


}

/**
 * Factory Handle The Details of Object Creation.
 * Not an actual design pattern, it just a programming idiom. This is the factory 
 * where we can create pizza. it should be the only part in our application that
 * refers to concrete Pizza classes. This decouples the client code in the
 * superclass from the object creation code in the subclass.
 * 
 * The Create method is often declared statically. 
 */