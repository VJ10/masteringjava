import java.util.Observer;
import java.util.Observable;

public class CurrentConditionDisplay implements Observer, IDisplay{
    private float temperature;
    private float humidity;
    Observable observable;
    
    // private float pressure; // dont need it

     // they need to have a reference to the subject so that they can register and remove themselves.

    public CurrentConditionDisplay(Observable obs){
        this.observable= obs;
        obs.addObserver(this);
    }

    // Pull => The update take both an Observable and the optimal data argument from notifyObservers(Object arg)
    public void update(Observable obs, Object arg){
        if(obs instanceof WeatherData){
            WeatherData weatherData = (WeatherData) obs;
            this.temperature = weatherData.getTemperature();
            this.humidity = weatherData.getHumidity();
            display();
        }
    }

    public void Unsubscribe(){
        observable.deleteObserver(this);
    }

    public void display(){
        System.out.println("Current Condition: " + temperature + " F degrees and humidity " + humidity +  " % humidity");
    }
}
/**
 * Pull
 *      Get the required data exactly through getters. 
 *      Can directly access Observable object in Update. and get whatever it wants. 
 *      More flexibily but the observers have to knows eveyrthing about the subject in order to query the right information
 *      if should be used when there are 2-3 different types of observers. 
 *      (different types means observer require different data)
 */