import java.util.Observable;
import java.util.Observer;

public class StatisticsDisplay implements Observer, IDisplay {

    Observable observable;
    WeatherData weatherData;

    public StatisticsDisplay(Observable obs) {
        this.observable = obs;
        observable.addObserver(this);
    }

    public void display() {
        System.out.println("Stats Page Avg. " + weatherData.getHumidity());
    }


    @Override
    public void update(Observable o, Object arg) {
        if(arg instanceof WeatherData){
            WeatherData updatedData = (WeatherData) arg;
            this.weatherData = updatedData;
            display();
        }
    }

}

/**
 * Push
 *      Observer get data wrapped in an object (Object arg) and needs to extract it.
 *      Cons : less flexibility
 *      can be used if there are 2-3 max differnt types of observers and they have different types.
 *      (different types means observer require different data)
 */