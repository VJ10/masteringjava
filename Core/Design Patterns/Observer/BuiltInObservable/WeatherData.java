import java.util.Observable;

public class WeatherData extends Observable{

    private float temperature;
    private float humidity;
    private float pressure;

    public WeatherData(){ }


    public void measurementChangedPull(){
        setChanged(); // From Observable class. This will call all of my current Observers.
        notifyObservers(); // Since we arent sending data object with the notifyObservers(). It means that we are using the pull MODEL
    }

    public void measurementChangedPush(WeatherData data){
        setChanged();
        notifyObservers(data); // If we want to push data then we must pass the data as data object. This will then be pass in the Object arg in the UPDATE() interface.
    }


    // Get Measurements From The Web.
    public void setMeasurementsPull(float temp, float hum, float press){
        this.temperature = temp;
        this.humidity = hum;
        this.pressure = press;
        measurementChangedPull();
    }


    public void setMeasurementsPush(float temp, float hum, float press){
        WeatherData nWeatherData = new WeatherData();
        nWeatherData.temperature = temp;
        nWeatherData.humidity = hum;
        nWeatherData.pressure = press;
        measurementChangedPush(nWeatherData);
    }

    // getters : there just in case the client wants to pull data. Observers uses them to get WeatherData Object state.
    public float getTemperature() {
        return temperature;
    }
    
    public float getHumidity() {
        return humidity;
    }
    
    public float getPressure() {
        return pressure;
    }

    public String toString(){
        return " The Humidity is " + this.humidity;
    }
}

/**
 * Observable class keepss tracks of all observer and notifies them when theres changes. So All observers must extend to it. ITS A CLASS NOT INTERFACE
 * 
 */