
public class WeatherStation{
    public static void main(String[] args) {

        // Subject : Publisher
        WeatherData weatherDataPull = new WeatherData();

        // Subscribers : Consumers
        CurrentConditionDisplay currentDisplay = new CurrentConditionDisplay(weatherDataPull);        
        weatherDataPull.setMeasurementsPull(45, 65, 34.f);



        // Push
        WeatherData dataPush = new WeatherData();
        StatisticsDisplay statisticsDisplay = new StatisticsDisplay(dataPush);
        dataPush.setMeasurementsPush(56f,76f,34f);
        
        // weatherData.deleteObserver(statsDisplay);
    }
}

/**
 * Observer : Defines one to many dependency between teh object so when one object changes state, all dependents are notified and updated automatically.
 * Observable Object : The Object that is being Observed. The state that we need. The Publisher
 * Observer : Objects that observer the observable objects. The consumers, subscribers.
 * Communication Mechanism :  Pull or Push
 * ALWAYS THE RESPONSIBILITY OF THE OBSERVABLE OBJECT TO NOTIFY ALL SUBSCRIBE OBSERVER BUT THE DIFFERENCE LIES WHETHER IS PULL OR PUSH.
 * https://stackoverflow.com/questions/34706186/push-pull-mechanism-observer-pattern
 */