package Implementation.Display;

import Implementation.Interface.DisplayElement;
import Implementation.Interface.Observer;
import Implementation.Interface.Subject;

public class CurrentConditionDisplay implements Observer, DisplayElement{
    private float temperature;
    private float humidity;
    private float pressure;
    private Subject weatherData;

    public CurrentConditionDisplay(Subject weatherData){
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    // The params was filled up by the subject all they have to do is copy it and use it as their own state.
    public void update(float temperature, float humidity, float pressure ){
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        display();
    }

    public void display(){
        System.out.println("Current Condition: " + temperature + "F degrees and humidity " + humidity +  "% humidity");
    }
}