package Implementation.Display;

// import Implementation.Interface.*;
import Implementation.WeatherData;
import Implementation.Interface.DisplayElement;
import Implementation.Interface.Observer;

public class StatisticsDisplay implements Observer, DisplayElement{

    float temperature;
    float humidity;
    float pressure;
    float result;
    WeatherData weatherData;

    public StatisticsDisplay(WeatherData weatherData){
        this.weatherData = weatherData;
        weatherData.registerObserver(this);        
    }

    public void update(float temp, float humidity, float pressure){
        this.result = (temp + pressure + humidity) / 3;
        display();
    }

    public void display(){
        System.out.println("Stats Page Avg. " + this.result);
    }

}