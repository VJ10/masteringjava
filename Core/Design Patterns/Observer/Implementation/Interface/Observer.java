package Implementation.Interface;

// All the subcribers need need is the updated data. That's all.
/**
 * Implemented by all Observer so they all have to implement the Update() method. Passing
 * The Measurement to the Observer. The Subjectwill use this and pass their state 
 * to the params and the observers will just read the data.
 */
public interface Observer{
    public void update(float temp, float humidity, float pressure);
}