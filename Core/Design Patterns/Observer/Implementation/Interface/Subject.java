package Implementation.Interface;

// They need to share the state. They are the newspaper they also need to keep tract of the subscriber.
public interface Subject{
 public void registerObserver(Observer o);
 public void removeObserver(Observer o);
 public void notifyObserver();
}

