package Implementation;

import java.util.ArrayList;

import Implementation.Interface.Observer;
import Implementation.Interface.Subject;

// This is Subject Since they are the class that need to share their state. 1 to Many.

public class WeatherData implements Subject{
    private ArrayList observers;
    private float temperature;
    private float humidity;
    private float pressure;

    public WeatherData(){
        observers = new ArrayList<>(); // need to keep track fof all the objervers.
    }

    public void registerObserver(Observer o){
        System.out.println("Observer " + o.getClass().getSimpleName() + " has been added.");
        observers.add(o);
    }

    public void removeObserver(Observer o){
        int i = observers.indexOf(o);
        if(i>=0) {
            System.out.println("Observer " + observers.get(i).getClass().getName() + " has been removed.");
            observers.remove(i);
        }
        
    }
    

    public void notifyObserver(){
        for(int i =0; i<observers.size(); i++){
            Observer obs = (Observer) observers.get(i);
            // so observers will implement this on their own. Loosely Coupled. Only pass the current data. Fill up the parameters. All the objerver have to to is print it.
            obs.update(temperature, humidity, pressure);  
        }
    }


    public void measurementChanged(){
        notifyObserver();
    }

    // Get Measurements From The Web.
    public void setMeasurements(float temp, float hum, float press){
        this.temperature = temp;
        this.humidity = hum;
        this.pressure = press;
        measurementChanged(); // since we change our data. It can get updated.
    }
}