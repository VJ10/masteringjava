package Implementation;

import Implementation.Display.CurrentConditionDisplay;
import Implementation.Display.HeatIndexDisplay;
import Implementation.Display.StatisticsDisplay;

public class WeatherStation {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();

        CurrentConditionDisplay currentDisplay = new CurrentConditionDisplay(weatherData);
        StatisticsDisplay statsDisplay = new StatisticsDisplay(weatherData);
        HeatIndexDisplay heatDisplay = new HeatIndexDisplay(weatherData);

        weatherData.setMeasurements(45, 65, 34.f);
        weatherData.removeObserver(statsDisplay);

        weatherData.setMeasurements(56, 76, 34.f);
    }
}