import  java.io.File;
import java.io.IOException;

public class CreateNewFile{

    public static void main(String[] args) {
        String fileSeperator = System.getProperty("file.separator");
        String absolutePath = "File IO" + fileSeperator + "test.txt";
        
        File file = new File(absolutePath);
        if(file.createNewFile()){
            System.out.println(absolutePath + " File Created. ");
        }else{
            System.out.println("File "  + absolutePath + " already exists!");
        }
    }
}