public class Guitar{
    private String serialNumber;
    private double price;
    private GuitarSpec guitarSpec;

    
  public Guitar(String serialNumber, double price, GuitarSpec spec) {
    this.serialNumber = serialNumber;
    this.price = price;
    this.guitarSpec = spec;
}

    public String getSerialNumber(){
        return this.serialNumber;
    }

    public double getPrice(){
        return this.price;
    }

    public void setPrice(double price){
        this.price = price;
    }

    public GuitarSpec getGuitarSpec(){
        return this.guitarSpec;
    } 

}