import java.util.List;

public class GuitarTester{
    
    public static void main(String[] args) {
        Inventory inventory = new Inventory();
        
        GuitarSpec erin = new GuitarSpec(Builder.FENDER, "Taylor", Type.ACOUSTIC, Wood.MAPLE, Wood.BRAZILIAN_ROSEWOOD);
        Guitar whatErinLikes = new Guitar("1serialNumber3", 13.13, erin);

        GuitarSpec Joe = new GuitarSpec(Builder.GIBSON, "Jow", Type.ELECTRIC, Wood.MAHOGANY, Wood.SITKA);
        Guitar whatJoeLikes = new Guitar("1serialber3", 34.41, Joe);

        GuitarSpec Casey = new GuitarSpec(Builder.MARTIN, "Casey", Type.ACOUSTIC, Wood.CEDAR, Wood.ALDER);
        Guitar whatCaseyLikes = new Guitar("1salNumber3", 54.23, Casey);

        GuitarSpec Maret = new GuitarSpec(Builder.COLLINGS, "Marvel", Type.ELECTRIC, Wood.INDIAN_ROSEWOOD, Wood.ADIRONDACK);
        Guitar whatMaretLikes = new Guitar("1serialNber3", 32.46, Maret);

        inventory.addGuitar(whatErinLikes);
        inventory.addGuitar(whatJoeLikes);
        inventory.addGuitar(whatCaseyLikes);
        inventory.addGuitar(whatMaretLikes);

        List<Guitar> matchingGuitars = inventory.search(Joe);

        System.out.println(inventory.toString());
        System.out.println("Size of " + matchingGuitars.size());
    }
}