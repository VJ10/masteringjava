import java.util.*;

public class Inventory{
    private List <Guitar> guitars;
   
    public Inventory(){
        this.guitars = new LinkedList<>();
    }
    
    public void addGuitar(String serialNumber, double price, GuitarSpec spec){
        Guitar gt1 = new Guitar(serialNumber, price, spec);
        guitars.add(gt1);
    }

    public void addGuitar(Guitar guitar){
        guitars.add(guitar);
    }

    

    public List<Guitar> search(GuitarSpec searchSpec){
        List <Guitar> matchinGuitars = new LinkedList<>();

        for(Iterator i = guitars.iterator(); i.hasNext();){
            
            Guitar guitar = (Guitar)i.next();
            GuitarSpec guitarSpec = guitar.getGuitarSpec();


            if(searchSpec.getBuilder() != guitarSpec.getBuilder()){
                continue;
            }

            String model = guitarSpec.getModel();
    
            if((model != null) && (!model.equals("") && (!model.equals(searchSpec.getModel()))))
            continue;

            if(guitarSpec.getType() != searchSpec.getType())
            continue;

            if(guitarSpec.getBackWood() != searchSpec.getBackWood())
            continue;

            if (guitarSpec.getTopWood() != searchSpec.getTopWood())
            continue;


            matchinGuitars.add(guitar);

        }

        return matchinGuitars;
    }

    public String toString(){
        return "INIT SIZE " +  this.guitars.size();
    }

    
}