public enum Wood { 

    INDIAN_ROSEWOOD, BRAZILIAN_ROSEWOOD, MAHOGANY, MAPLE,
    COCOBOLO, CEDAR, ADIRONDACK, ALDER, SITKA;
  
    public String toString() {
      switch(this) {
        case INDIAN_ROSEWOOD:    return "Indian Rosewood";
        case BRAZILIAN_ROSEWOOD: return "Brazilian Rosewood";
        case MAHOGANY:           return "Mahogany";
        case MAPLE:              return "Maple";
        case COCOBOLO:           return "Cocobolo";
        case CEDAR:              return "Cedar";
        case ADIRONDACK:         return "Adirondack";
        case ALDER:              return "Alder";
        case SITKA:              return "Sitka";
        default:  return "unspecified";
      }
    }
  }
  

  /**
   * Enum are Enumerated Types
   * They let you define a type name and ten set of values that are allowed for that type
   * Then we can refer to the specific value like Wood.CEDAR
   * Its great to get type safety and also avoid getting bad data for anything that has a standard range or set of legal values
   */