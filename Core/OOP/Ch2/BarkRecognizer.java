public class BarkRecognizer{
    private DogDoor door = new DogDoor();

    public BarkRecognizer(DogDoor door){
        this.door = door;
    }
    
    public void recognize(String bark){
        // When This Is True meaning recognize the dog is barking tehn can saefely ofpen the doot
        System.out.println(" BarkRecognizer: Heard a '" + bark + "''");
        door.isOpen();
    }
}