import java.util.Timer;
import java.util.TimerTask;

public class DogDoor
{
    private boolean isOpen;

    public DogDoor(){
        this.isOpen = false;
    }

    // Door Should Close Automatically
    
    public void open(){
        this.isOpen = true;
        System.out.println("Door Opens.");

        // Use to schedule the opening and closing of the door. The task closes the door and then turn off the timer.
        final Timer timer = new Timer();
        timer.schedule(new TimerTask(){
            public void run(){
                close();
                timer.cancel(); 
            }
        },5000);  // Wait 5000 milliseconds before executing the task.

    }

    public void close(){
        this.isOpen = false;
        System.out.println("Door Closes.");
    }

    public boolean isOpen(){
        return this.isOpen;
    }
}

/**
 * Sometime Change in Requirement can reveal problems with system that we didnt even know were there.
 * Change is constance and system should always improve everytime we work on it.
 * Good Requirement ensure that system works like customers expects, we should make sure that req covers all steps in the
 * use cases for system. ENCAPSULATE WHAT VARIES => Helped us realized that the dog should handle closing itself.
 */