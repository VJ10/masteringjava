class DogDoorStimulator{
    public static void main(String[] args) {
        DogDoor door = new DogDoor();
        BarkRecognizer recognizer = new BarkRecognizer(door);
        Remote remote = new Remote(door);


        System.out.println("Fido Starts Barking");
        recognizer.recognize("Woof");
        System.out.println("\n Fido has gone outside...");
        System.out.println("\n Fido's all done..");
        System.out.println("\n Fido is Now Stuck Outside");

        /*
        System.out.println("\n Fido has gone outside...");
        // remote.pressButton();
        System.out.println("\n Fido's all done..");
        // remote.pressButton();
        System.out.println("\n Fido back inside ...");
        // remote.pressButton();
        */
    }
}