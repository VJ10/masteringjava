public class Bark{
    private String sound;

    public Bark(String sound){
        this.sound = sound;
    }

    public String getSound(){
        return sound;
    }

    public boolean equals(Object otherBark){
        Bark newObj = (Bark) otherBark;
        return this.sound == newObj.getSound();
    }
}

/**
 * The power of loosely coupled application
 * Delegation helps application stay loose meaning that objects are independent of each other.
 * One change in another object does not require to make bunch of changes to other objects.
 */