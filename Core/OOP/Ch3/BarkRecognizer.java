public class BarkRecognizer{
    private DogDoor door = new DogDoor();

    public BarkRecognizer(DogDoor door){
        this.door = door;
    }

    // Delegate back comparison to Bark.
    public void recognize(Bark bark){
        // When This Is True meaning recognize the dog is barking tehn can saefely ofpen the doot
        System.out.println(" BarkRecognizer: Heard a '" + bark.getSound() + "''");
        if(door.getAllowedBark().equals(bark)){
            door.open();
        }else{
            System.out.println("Dog Is Not Allowed");
        }
        

       
    }
}

/**
 * This take account of all dogs in the neighborhood.
 * By delagating the comparison of barks to the bark object, we abstract teh details about what make two barks the same away from the 
 * bark recognizer class
 * 
 * Now let stay if we want to use an audio file to actually do the comparison between barks, we wont have to update much just the 
 * equals in teh bark object. With delegation and loosely coupled application we can change the implementation of one object like
 * bark and wont have to change other object in the application. Objects are shielded from implementation changes in other objects.
 */