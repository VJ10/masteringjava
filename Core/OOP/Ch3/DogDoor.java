import java.util.Timer;
import java.util.TimerTask;

public class DogDoor
{
    private boolean isOpen;
    Bark allowedBark;

    public DogDoor(){
        this.isOpen = false;
    }

    public void setAllowedBark(Bark bark){
        this.allowedBark = bark;
    }

    public Bark getAllowedBark(){
        return allowedBark;
    }

    // Door Should Close Automatically
    public void open(){
        this.isOpen = true;
        System.out.println("Door Opens.");

        // Use to schedule the opening and closing of the door. The task closes the door and then turn off the timer.
        final Timer timer = new Timer();
        timer.schedule(new TimerTask(){
            public void run(){
                close();
                timer.cancel(); 
            }
        },5000);  // Wait 5000 milliseconds before executing the task.

    }

    public void close(){
        this.isOpen = false;
        System.out.println("Door Closes.");
    }

    public boolean isOpen(){
        return this.isOpen;
    }
}

/**
 * The first step is to figure out potential problems. we know that problems occurs when there are multiple dogs in the same neighborhood.
 */