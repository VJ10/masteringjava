public class Instrument{

    private String serialNumber;
    private double price;
    private InstrumentSpec instrumentSpec;  // Using Aggreagation because each instrument is made up of serialNum an dPrice and spec


    public Instrument(String serialNumber, double price, InstrumentSpec spec){
        this.serialNumber = serialNumber;
        this.price = price;
        this.instrumentSpec = spec;
    }

    // Getters And Setters
    public String getSerialNumber(){
        return serialNumber;
    }

    public double getPrice(){
        return price;
    }

    public void setPrice(float price){
        this.price = price;
    }

    public InstrumentSpec getInstrumentSpec(){
        return instrumentSpec;
    }

}

/**
 * Took all attributes and operations that are common to both Guitar and Mandolin
 * and put them in Instruments. They are placeholders for actual implementation. it defines
 * behavior and subclasses implement the behavior. The reason that we use abstract classes
 * is all about behavior. we create subclass because the behavior od the subclass is
 * different than the subclass.
 * 
 * All the instruments behave the same. we have subclasses.
 * 
 * We used aggregation form of association because each instrument is made up of the 
 * serial number and price member variable and an instrumentSpec instance.
 * 
 * Encapsulation => HELP PROTECT CLASSES FROM UNCECESSARY CHANGES
 *  is responsible for preventing more maintenace problems than any other OO
 * principle in history by localizinf the changes for the behavior of an object to vary.
 */