import java.util.HashMap;

public class InstrumentSpec {
    private HashMap<String, Object> properties;
    private String model;
    private Builder builder;
    private Type type;
    private Wood topWood;
    private Wood backWood;

    public InstrumentSpec(HashMap <String, Object> properties){
        if(properties == null){
            this.properties = new HashMap<>();
        }else{
            this.properties = new HashMap<>(properties);
        }
        // this.builder = builder;
        // this.model = model;
        // this.type = type;
        // this.backWood = backWood;
        // this.topWood = topWood;
    }

    public Object getProperty(String propertyName){
        return properties.get(propertyName);
    }

    public HashMap<String, Object> getProperties(){
        return properties;
    }

    public boolean matches(InstrumentSpec spec){
       
        if(builder != spec.builder){
            return false;
        }

        if((model != null) && (!model.equals("")) && (!model.equals(spec.model))){
            return false;
        }

        if(type != spec.type){
            return false;
        }

        if (this.backWood != spec.backWood){
            return false;
        }

        if(this.topWood != spec.backWood){
            return false;
        }

        return true;
        
    }
}

/**
 * Since so many instrument have common specifications.
 */