import java.util.*;

public class Inventory{
    private List<Instrument> instruments;
   
    public Inventory(){
        this.instruments = new LinkedList<>();
    }
    
    public void addInstrument(String serialNumber, double price, InstrumentSpec spec){
        Instrument e = new Instrument(serialNumber, price, spec);
        instruments.add(e);
    }

    public void addInstrument(Instrument name){
        instruments.add(name);
        System.out.println("Added Instrument " + name.getSerialNumber());
    }

    public Instrument get(String serialNumber){
        for(Iterator i = instruments.iterator(); i.hasNext();){
          Instrument instrument = (Instrument)i.next();
          if(instrument.getSerialNumber().equals(serialNumber)){
              return instrument;
          }  
        }

        return null;
    }

    public List<Instrument> search(InstrumentSpec searchSpec){
        List <Instrument> matchingInstruments = new LinkedList<Instrument>();
        for(Iterator i = instruments.iterator(); i.hasNext();){
            Instrument instrument = (Instrument) i.next();
            if(instrument.getInstrumentSpec().matches(searchSpec)){
                matchingInstruments.add(instrument);
            }
        }
        return matchingInstruments;
    }

    public String toString(){
        return "INIT SIZE " +  this.instruments.size();
    }

    
}

/**
 *                  DONT LIMIT YOURSELF
 * Coding to an interface rather thatn an implementation make software
 * easier to extedn. By coding to inteface code will work with all
 * of the interface subclasses even the one that havent been created yet.
 * 
 * SOLID THIS SHOULD ONLY ADD, SEARCH, REMOVE FROM INVENTORY
 */