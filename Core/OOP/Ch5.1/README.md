Good Design = Flexible Software
Nothing ever stays the same. Change is inevitable no matter how much you like your software at the moment.

We made instrument abstract since we abstracted away properties common to Guitar and Mandoln. there is no such things as actual instrument all it does is
provide a common place to store properties that exists in both guitar and mandolin classes. used to define common attributes and properties that all 
instrument need to implement.

Whenever we find common behavior in two or more place, look for abstract that behavior into a class and then reuse that behavior in common classes.