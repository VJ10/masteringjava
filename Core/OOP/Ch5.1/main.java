import java.util.HashMap;

class Main{
    public static void main(String[] args) {

        Inventory inventory =  new Inventory();

        // Guitar Spec
        HashMap <String, Object> guitarSpec = new  HashMap<String, Object>();
        guitarSpec.put("builderName", Builder.COLLINGS);
        guitarSpec.put("model", "Taylor XIII");
        guitarSpec.put("type", Type.ELECTRIC);
        guitarSpec.put("backwood", Wood.CEDAR);
        guitarSpec.put("topwood", Wood.CEDAR);

        // Banjo Spec
        HashMap<String, Object> banjoSpec = new HashMap<>();
        banjoSpec.put("builderName", Builder.FENDER);
        banjoSpec.put("model", "Femder Bender III");
        banjoSpec.put("type", Type.ACOUSTIC);
        banjoSpec.put("backwood", Wood.SITKA);
        banjoSpec.put("topwood", Wood.INDIAN_ROSEWOOD);

        // Creating Instrument Spec
        InstrumentSpec aSpec = new InstrumentSpec(guitarSpec);
        InstrumentSpec bSpec = new InstrumentSpec(banjoSpec);

        // Creating Instruments
        Instrument guitar = new Instrument ( "124545", 14.22, aSpec );
        Instrument banjo = new Instrument("42354353", 5454.22, bSpec);


        // Adding Instruments
        inventory.addInstrument(guitar);
        inventory.addInstrument(banjo);
        System.out.println(inventory);
        
        
    }
}