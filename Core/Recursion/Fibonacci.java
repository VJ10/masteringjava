class Fibonacci{

    public int Fib(int num){
        if (num == 0){
            return 1;
        }

        if (num == 1){
            return 1;
        }
        
        return Fib(num-1) + Fib(num-2);
    }
    public static void main(String[] args) {
        Fibonacci a = new Fibonacci();
        
         System.out.println(a.Fib(2));
    }

    
}