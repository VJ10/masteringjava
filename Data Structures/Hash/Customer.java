import java.util.*;

class Customer{
    private long id;
    private String name;

    public Customer(long id, String name){
        this.id = id;
        this.name = name;
    }

    public void setId(long id){
        this.id = id;
    }
    public long getId(){
        return id;
    }

    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }

    // Two Customer Are Equal if their ID are equal
    public boolean equals(Object o){
        if (this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer)o;
        return id == customer.id;
    }

    public int hashCode(){
        return Objects.hash(id);
    }

    public String toString(){
        return "Customer{id="+ id + ", name'=" + name + '\'' + '}';
    }

}
