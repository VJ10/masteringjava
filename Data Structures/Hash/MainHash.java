import java.util.HashSet;
import java.util.Set;

class MainHash{
    public static void main(String[] args) {
        HashSet <String> wdsSet = new HashSet<>();
        wdsSet.add("String Added");
        wdsSet.add("Messi Was Here");
        wdsSet.add("Basic List But Built On HashTable");
        wdsSet.add("In The Implementation, it o(1) to add");

        for (String wrd : wdsSet) {
            System.out.println(wrd);
            System.out.println("======");
        }

        System.out.println("The Size: " + wdsSet.size());
        System.out.println("Removing All Items");
        wdsSet.clear();
        System.out.println("New Set: " + wdsSet.size());

        System.out.println("Bit More Complex");
        Set<Customer> customers = new HashSet<>();
        customers.add(new Customer(101,"Andrew W")); /**Hashet will use the `equals()` & `hashCode()` implementation of the Customer Class to check for dupes and ignore them */
        customers.add(new Customer(102, "Andrea R"));
        customers.add(new Customer(103, "Serena S"));
        customers.add(new Customer(1010,"Lionel M"));

        System.out.println(customers);
    }
}

/**
 * HashSet Stores unique elements and permit nulls. Only One Tho
 * it backed by HashMap, doesnt maintain insertion order and not thread safe.
 * 
 * When we add an abject into a HashSet it uses the object hashcode value, to determine
 * if an element is not in the set already.  Each Hash code value corresponds to a cetain bucket location which can contain various elements
 * But Two Objects with the same hashCode might not be equal. So Object withing the same bucket will be compaires using the equals() method
 */