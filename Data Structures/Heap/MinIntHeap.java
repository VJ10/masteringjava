import java.util.Arrays;

public class MinIntHeap{

    int [] items;
    private int capacity;
    private int size = 0;

    public MinIntHeap(int capacity){
        this.capacity = capacity;
        items = new int[capacity];
    }

    public int getMinIntHeap(int index){
        int currentItem = -1;

        if(index>= 0 && index < size){
            currentItem = items[index];
        }
        
        return currentItem;
    }
    
    /****************************************************
     *                       Value                      *
     ****************************************************/

    public int getLeftChild(int parentIndex){
        int location = (parentIndex * 2) + 1;
        return items[location];
    }

    public int getRightChild(int parentIndex){
        int location = (parentIndex * 2) + 2;
        return items[location];
    }
    
    public int getParent(int childrenIndex){
        int location = (childrenIndex - 1)/2;
        return items[location];
    }
     
    /****************************************************
     *                  Actual Index                    *
     ****************************************************/

    public int getParentIndex(int childrenIndex){
        return (childrenIndex - 1)/2;
    }

    public int getLeftChildIndex(int parentIndex){
        return (parentIndex * 2) + 1; 
    }

    public int getRightChildIndex(int parentIndex){
        return (parentIndex * 2) + 2;
    }

    /****************************************************
     *                  YES OR NO                       *
     ****************************************************/
    private boolean hasLeftChild(int index){
        return getLeftChildIndex(index) <= size;
    }

    private boolean hasRightChild(int index){
        return getRightChildIndex(index) <= size;
    }

    private boolean hasParent(int index){
        return getParentIndex(index) >=0 ;
        // return items[getParent(index)] >= 0;
    }

    /****************************************************
     *                 HELPERS                          *
     ****************************************************/


    public int less(int i, int j){
        int result;

        if(items[j] > items[i]){
            result = i;
        }else{
            result = j;
        }
        return result;
    }
     // less 
     // greater

    private void swap(int indexOne, int indexTwo){
        int res = items[indexOne];
        items[indexOne] = items[indexTwo];
        items[indexTwo] = res;
    }

    private void ensureExtraCapacity(){
        if(size == capacity){
            items = Arrays.copyOf(items, size*2);
            capacity *= 2;
        }
    }

    private int peek(){
        if(size ==0) throw new IllegalStateException("Array Is Empty.");
        return items[0];
    }

    private int poll(){
        if (size == 0) throw new IllegalStateException();

        int removeItem = items[0];
        items[0] = items[size-1];
        
        size--;

        heapifyDown();
        return removeItem;
    }

    public void insert(int item){
        ensureExtraCapacity();
        items[size] = item;
        heapifyUp();
        size++;
    }

    public void heapifyUp(){
        int index = size;
        while(hasParent(index) && items[index] < getParent(index)){
            swap(getParentIndex(index), index);
            index = getParentIndex(index);
        }
    }

    public void heapifyDown(){
        
        int currentRootIndex = 0;
        int currentRootNode = items[currentRootIndex];

        while( hasLeftChild(currentRootIndex) || hasRightChild(currentRootIndex) ){
            
            if(hasLeftChild(currentRootIndex) && hasRightChild(currentRootIndex) && currentRootNode > getLeftChild(currentRootIndex) && currentRootNode > getRightChild(currentRootIndex) ){
                int minVal = less(getLeftChildIndex(currentRootIndex), getRightChild(currentRootIndex));
                swap(minVal, currentRootIndex);
                currentRootIndex = minVal;
            }
            
            if(hasLeftChild(currentRootIndex) && currentRootNode > getLeftChild(currentRootIndex)){
                swap(getLeftChildIndex(currentRootIndex), currentRootIndex);
                currentRootIndex = getLeftChildIndex(currentRootIndex);
            }

            if(hasRightChild(currentRootIndex) && currentRootNode > getRightChild(currentRootIndex) ){
                swap(getRightChildIndex(currentRootIndex), currentRootIndex);
                currentRootIndex = getRightChildIndex(currentRootIndex);
            }

            // Then Just Change The Root Node Again.
            break;
        }

    }

    public void view(){
        System.out.println("Current Size: " + size + '/' + capacity);
        for(int i = 0; i < size; i++){
            System.out.print(items[i] + " | ");
        }
        System.out.println();
    }
    public static void main(String[] args) {
        MinIntHeap mh = new MinIntHeap(5);

        mh.insert(6);
        mh.insert(4);
        mh.insert(5);
        System.out.println( "Peeking  " +   mh.peek());
        
        
        mh.insert(2);
        mh.insert(3);
        mh.insert(1);
        System.out.println( "Peeking  " +   mh.peek());


        mh.view();

        System.out.println( "Removing Item  " +   mh.poll());
        System.out.println( "Removing Item  " +   mh.poll());
        System.out.println( "Removing Item  " +   mh.poll());
        
        mh.view();
        
    }


}
