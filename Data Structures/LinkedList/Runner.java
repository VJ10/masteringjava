import SimpleLinkedList.*;

public class Runner {

    // Reverse Linked List

    public static void main(String[] args) {

        // 1->2->3->4->5->NULL
        // LinkedListNode<Integer> One = new LinkedListNode<Integer>(1);
        LinkedList<Integer> linkedLst = new LinkedList<Integer>();
        linkedLst.appendNode(new LinkedListNode<Integer>(1));
        // linkedLst.appendNode(new LinkedListNode<Integer>(2));
        // linkedLst.appendNode(new LinkedListNode<Integer>(3));
        // linkedLst.appendNode(new LinkedListNode<Integer>(4));

        linkedLst.printList(linkedLst.getHead());

        // LinkedListNode<Integer> two = new LinkedListNode<Integer>(2);
        // LinkedListNode<Integer> threeNode = new LinkedListNode<Integer>(3);
        // LinkedListNode<Integer> four = new LinkedListNode<Integer>(4);
        // LinkedListNode<Integer> five = new LinkedListNode<Integer>(5);

        // One.next = two;
        // two.next = threeNode;
        // threeNode.next = four;
        // four.next = five;
        // five.next = null;

        // LinkedListNode<Integer> currNode = One;
        // LinkedListNode<Integer> prevNode = null;
        // LinkedListNode<Integer> nxtNode = null;

        // while (currNode != null) {

        // nxtNode = currNode.next;

        // // System.out.println("Curr Node: " + currNode.value);
        // // System.out.println("Next Node: " + nxtNode.value);
        // // System.out.println("Prev Node: " + prevNode.value);
        // // System.out.println("============================");

        // // Set Current Nxt Node To Prev Node
        // currNode.next = prevNode;

        // prevNode = currNode;
        // currNode = nxtNode;

        // }

        // System.out.println(prevNode);

        // String[] a = { "Mas", "club", "FCB", "bar" };
        // String[] b = { "Espa", "Blanc", "Suar", "Bertr", "FCB" };

        // for (int i = 0; i < a.length; i++) {
        // for (int j = i; j < b.length; j++) {
        // if (a[i] == a[j]) {
        // System.out.println("found it");
        // break;
        // }
        // System.out.println("not found");
        // }

        // }
    }
}