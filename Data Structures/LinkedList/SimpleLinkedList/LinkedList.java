package SimpleLinkedList;

import java.util.ArrayList;

public class LinkedList<T> {

    private LinkedListNode<T> head = null;
    private LinkedListNode<T> tail = null;
    int size = 0;

    public LinkedList(LinkedListNode<T> node) {
        this.head = node;
    }

    public LinkedList() {
        this.head = null;
        this.tail = null;
    }

    public LinkedListNode<T> getHead() {
        return this.head;
    }

    public void appendNode(LinkedListNode<T> newNode) {
        LinkedListNode<T> prevNode;

        if (head == null) {
            head = newNode;
        }

        while (head != null) {
            
            if (head.next == null) {
                prevNode = head;
                prevNode.next = newNode;
            }

            head = head.next;
        }

    }

    /**
     * insertFront At The Front Of The list
     * 
     * @param node
     */
    public void insertFront(LinkedListNode<T> newNode) {

        if (head == null && tail == null) {
            head = newNode;
            tail = newNode;
            size++;
        } else {
            newNode.setNext(head);
            head = newNode;
            size++;
        }
    }

    /**
     * insertEnd At The End Of The list
     * 
     * @param node
     */
    public void insertEnd(LinkedListNode<T> newNode) {
        if (tail == null) {
            tail = newNode;
        }
        tail.next = newNode;
        newNode.next = null;
        tail = newNode;
        size++;

        // LinkedListNode<T> currHead = head;
        // while(currHead.next != null) currHead = currHead.next;
        // currHead.next = newNode;
        // newNode.next = null;
    }

    public void getByIndex(int index) {
        int currentCounter = 0;
        LinkedListNode<T> currHead = head;

        if (index >= size) {
            throw new IllegalArgumentException("Reached Cap");
        }

        while (currHead != null) {
            if (index >= 0 && index == currentCounter) {
                System.out.println(currHead.getValue() + " ==> " + currentCounter);
                break;
            }

            currentCounter++;
            currHead = currHead.next;
        }
    }

    /**
     * insertIndex At any index except the head.
     * 
     * @param node
     */
    public void removeIndex(int index) {

        int count = 0;
        LinkedListNode<T> currHead = head;
        LinkedListNode<T> removeNode;

        if (head == null) {
            System.out.println("Empty");
        }

        // Remove Head
        if (index == 0) {
            // currHead = head;
            head = currHead.next;
            currHead.next = null;
            size--;
        }

        // Remove Tail
        if (index == this.size - 1) {
            // currHead = head;

            while (currHead.next.next != null) {
                currHead = currHead.next;
            }

            // Previous To Last One.
            this.tail = currHead;
            this.tail.next = null;
            size--;
        }

        // Anywhere from 1 to size-1
        if (index != 0 || index != this.size - 1 && index > 0) {
            while (currHead != null) {
                if (count == index - 1) {
                    removeNode = currHead.next;
                    currHead.next = currHead.next.next;
                    removeNode.next = null;
                    size--;
                }
                count++;
                currHead = currHead.next;
            }
        }

        // for(currHead = head; currHead!= null ; currHead = currHead.next){
    }

    /**
     * removeFront from the from of the list
     * 
     * @param node
     */
    public void removeFront() {
        if (head != null)
            head = head.getNext();
        else
            head = null;
    }

    public void printList(LinkedListNode<T> node) {
        System.out.println("Node is " + node.getValue());
        if (node.getNext() != null)
            printList(node.getNext());
    }

    public void print() {
        printList(head);
    }

    public void info() {
        System.out.println("Current Size: " + this.size);
        System.out.println("Head: " + this.head);
        System.out.println("Tail: " + this.tail);
    }

    /***************************************************************************
     * DUPLICATE
     ***************************************************************************/
    public boolean contains(T value) {
        LinkedListNode<T> currHead = head;
        while (currHead != null) {
            if (value.equals(currHead.value)) {
                return true;
            }
            currHead = currHead.next;
        }
        return false;
    }

    // public void duplicate(){
    // ArrayList <T> items = new ArrayList<>();
    // LinkedListNode<T> currHead = head;
    // int currentIndex = 0;

    // while(currHead != null){
    // if (!items.contains(currHead.value)){
    // items.add(currHead.value);
    // } else{
    // removeIndex(currentIndex);
    // System.out.println("Removing " + currentIndex + "==>" );
    // }
    // currentIndex++;
    // currHead = currHead.next;
    // }
    // System.out.println();
    // }

    // MAIN

    public static void main(String[] args) {
        // LinkedListNode<String> newHead = new LinkedListNode<String>("Matt");

        // LinkedList<String> list = new LinkedList<String>();
        // list.insertFront(newHead);
        // list.insertFront(new LinkedListNode<String>("Taylor"));
        // list.insertEnd(new LinkedListNode<String>("Christen"));
        // list.insertFront(new LinkedListNode<String>("Alex"));
        // list.insertFront(new LinkedListNode<String>("Taylor"));
        // list.insertEnd(new LinkedListNode<String>("Messi"));
        // list.insertFront(new LinkedListNode<String>("Leo"));

        // list.info();
        // System.out.println();

        // list.print();
        // System.out.println();

        // System.out.println();

        // list.removeIndex(2);

        // System.out.println("THE HEAD IS " + list.head.getValue());
        // list.removeFront();
        // System.out.println("After removing the head..");

    }
}

// ListNode