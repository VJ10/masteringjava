public class ExecuteMain{
    public static void main(String[] args) {
        System.out.println("Linked List Implementation of Queue");
        LinkedQueue items = new LinkedQueue();

        System.out.println("Adding Elements");
        items.enQueue("Messi");
        items.enQueue("Alex");
        items.enQueue("Christen");
        items.enQueue("Sydney");
        items.enQueue("Viktor");
        items.display();

        System.out.println("Removing Few Items");
        items.deQueue();
        items.deQueue();

        items.display();
    }
}