import java.util.LinkedList;
import java.util.*;

public class LinkedQueue{
    LinkedList <String> items = new LinkedList<String>();

    public void enQueue(String newItem){
        items.addLast(newItem);
    }

    public void deQueue(){
        items.removeFirst();
    }

    public void display(){
        Iterator <String> iterator = items.iterator();
        while(iterator.hasNext()){
            System.out.print(iterator.next() + " ==> ");
        }
        System.out.print("null");
        System.out.println();
    }
}