package TailRecursion;
public class Tail{
    public static int tailSum(int currSum,  int n){
        if (n <= 1) {
            return currSum + n;
        }

        return tailSum(currSum + n, n - 1);
    }

    public static void main(String[] args) {
        System.out.println("Tail Recursion");
        int res = tailSum(10, 3);
        System.out.println(res);
    }
}