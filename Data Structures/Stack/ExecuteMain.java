import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;
import java.util.ListIterator;

public class ExecuteMain{

    public static void reverseName(String name){
        LinkedList<Character> items=new LinkedList<Character>();
        StringBuilder returnString = new StringBuilder();
        
        
        for(int i = 0; i<name.length(); i++){
            items.push(name.charAt(i));
        }
        /** 
        while(!items.isEmpty()){
            items.removeIf(element -> element.equals('s'));
            returnString.append(items.pop());
        }
        System.out.println("Size " + items.size());        
        System.out.println(returnString.toString());
        */
        ListIterator<Character> stackIterator = items.listIterator();
        System.out.println("TEsting");
        while(stackIterator.hasNext()){
            System.out.println(stackIterator.next());
            System.out.println("QUE");
        }
    }
    public static void main(String[] args) {

        System.out.println("Linked List Implementation");
        LinkedStack a = new LinkedStack();
        a.push("13");
        a.push("32");
        a.push("37");
        a.push("95");
        a.push("10");

        System.out.println("After Adding Items");
        a.display();
      
        System.out.println("Removing Few Item");
        a.pop();
        a.pop();
        a.pop();

        a.display();

        System.out.println("Array Implementation");
        ArrayStack as = new ArrayStack();
        System.out.println(as.items);

        reverseName("Messi");
        
    }
}

/**
 * Can Implement stack is a collection with anything. Also we can use it with Streams.
 * Array, ArrayList, Linked List.
 * Each have their benefits, choose wisely.
 * 
 * Stack is a direct subclass of vector this its a synchronized implementation. however npt a;way needed.
 * the only difference between the addElement() and push() is that the result of the operation
 * instead of that element is added.
 * 
 * Staack alllows us to search for an lement and get ists distance from the top. Only have a few operation
 * empty(), peek, pop(), pusj() and search() => returns 1 based position when an object is on the stack
 */