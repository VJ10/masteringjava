import java.util.LinkedList;

public class LinkedStack{
    LinkedList<String> items=new LinkedList<String>();
    
    public void display(){
        System.out.println("Iterating Through Stack");
        for(String str : items){
            System.out.println(str);
        }
    }

    public String peek(){
        return items.get(0);
    }

    public void push(String newItem){
        items.addFirst(newItem);
    }

    public void pop(){
        items.removeFirst();
        System.out.println("Deleted " + items.getFirst());
    }
}