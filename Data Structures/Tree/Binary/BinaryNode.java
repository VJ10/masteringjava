package Binary;

class BinaryNode <T> {
    private T data;
    BinaryNode <T> leftChild;
    BinaryNode <T> rightChild;
    BinaryNode <T> parent;

    BinaryNode(T data, BinaryNode<T> parent){
        this.data = data;
        this.parent = parent ;
        this.leftChild = null;
        this.rightChild = null;
    }

    public void setData(T data){
        this.data = data;
    }

    public T getData(){
        return this.data;
    }

    public boolean hasLeftChild(){
        if(this.leftChild == null) {return false;}
        else{return true;}
    }

    public boolean hasRightChild(){
        if(this.rightChild == null) {return false;}
        else{return true;}
    }

    public void toStringGeneric(){
        System.out.println( this.getData());
    }

}