package Binary;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;
import java.util.Queue;

class BinaryTreeNode {

    /**-----------------------------
     *          Traversing
     *------------------------------*/
    static void traverse(BinaryNode <Integer> node){
        if (node == null){
            return;
        }else{
            System.out.println(node.getData());
            traverse(node.leftChild);
            traverse(node.rightChild);
        }
    }

     /**-----------------------------
     *          Depth First
     *------------------------------*/
    
    /**
     * Left Subtree => Current Node => Right SubTree
     * @param root
     */
    static void inOrderTraversal(BinaryNode <Integer> root){
        if (root == null){
            return;
        }else{
            inOrderTraversal(root.leftChild);
            System.out.println(root.getData());
            inOrderTraversal(root.rightChild);
        }
    }

    static void traverseIteratively(BinaryNode<Integer>root){
        Stack <BinaryNode<Integer>> items = new Stack<>();
        BinaryNode<Integer> currNode = root;
        ArrayList<BinaryNode<Integer>> seen = new ArrayList<>();

        items.push(root);

        while(!items.empty()){
            currNode = items.peek();
            
            if(currNode.hasLeftChild() & !seen.contains(currNode.leftChild)){
                items.push(currNode.leftChild);
                currNode = currNode.leftChild;
                continue;
            }

            if(currNode.hasRightChild() & !seen.contains(currNode.rightChild)){
                items.push(currNode.rightChild);
                currNode = currNode.rightChild;
                continue;
            }

            if(!currNode.hasLeftChild() & !currNode.hasRightChild() || seen.contains(currNode.leftChild) || seen.contains(currNode.rightChild)){
                System.out.println(currNode.getData());
                seen.add(currNode);
                items.pop();
            }
            
        }
        

    }

    /**
     * Current Node => Left Subtree => Right Subtree
     * @param root
     */
    static void preOrderTraversal(BinaryNode <Integer> root){
        if (root == null){
            return;
        }else{
            System.out.println(root.getData());
            preOrderTraversal(root.leftChild);
            preOrderTraversal(root.rightChild);
        }
    }

    /**
     * Left Subtree => Right Subtree => Current Node
     * @param root
     */
    static void postOrderTraversal(BinaryNode <Integer> root){
        if(root == null){
            return;
        }else{
            postOrderTraversal(root.leftChild);
            postOrderTraversal(root.rightChild);
            System.out.println(root.getData());
        }
    }

     /**-----------------------------
     *          Breadth First
     *------------------------------*/
    /**
     * the distance from the top or surface to the bottom of something.
     * Top to Bottom
     * @param root
     */
    static void depthFirstTraversal(BinaryNode <Integer> root){

    }

    /**
     * Breadth the distance or measurement from side to side of something; width.
     * Side to Side. It goes wide level by level. think of big circle.
     */
    static void breadthFirstTraversal(BinaryNode <Integer> root){
        BinaryNode <Integer> currentNode = root;
        Queue <BinaryNode> list = new LinkedList();
        int level = 0;

        list.add(currentNode);

        while(!list.isEmpty()){
            // Work with the first item in the line.
            BinaryNode<Integer> nextNode = list.poll();
            
            // Process it
            System.out.print(nextNode.getData());
            System.out.print(" => ");
            

            // Do They Have Children???
            if(nextNode.hasLeftChild()){
                list.add(nextNode.leftChild);
            }

            if(nextNode.hasRightChild()){
                list.add(nextNode.rightChild);
            }

            if(nextNode.hasLeftChild() || nextNode.hasRightChild()){
                level += 1;
            }            
        }

        System.out.println("Total Level : " );
        System.out.print(level-1);
    }    

    /**-----------------------------
     *          Searching
     *------------------------------*/

     static void searchIterativeBST(int key, BinaryNode <Integer> root){
         BinaryNode <Integer> currentNode = root;

         while (currentNode != null ){
             
            if (key == currentNode.getData() )  { 
                System.out.println("Found it " + currentNode.getData());
                break;
            }
             
            if (key < currentNode.getData()  )  {
                currentNode= currentNode.leftChild;
            }
            
            if (currentNode != null){
                if ( key > currentNode.getData()  )  {
                    currentNode = currentNode.rightChild; 
                }
            }

            
         }

        System.out.println("Not Found");
     }

     static void searchRecursiveBST(int key, BinaryNode <Integer> root){
         if (root == null) {
             return;
         }else{
            if(key == root.getData()) {System.out.println("Found Key " + root.getData());}
            if(key < root.getData()) searchRecursiveBST(key, root.leftChild);
            if(key > root.getData()) searchRecursiveBST(key, root.rightChild);  
         }

        System.out.println("Not Found");
     }

     static void searchMin(BinaryNode <Integer> root){
         BinaryNode <Integer> currentNode = root;
         while(currentNode.leftChild != null){
            currentNode = currentNode.leftChild;
            System.out.println("New Level");
         }

         System.out.println(currentNode.getData());
     }

     static void searchMax(BinaryNode <Integer> root){
         BinaryNode <Integer> currentNode = root;
         while(currentNode.rightChild != null){
             currentNode = currentNode.rightChild;
             System.out.println("Visiting New Level");
         }

         System.out.println(currentNode.getData());
     }

     // Adding
    //  static void addNewBinaryNode(Integer data, BinaryNode <Integer> root){
    //     BinaryNode<Integer> newNode = new BinaryNode<Integer>(data);
    //     BinaryNode <Integer> currentNode = root;
    //  }


     // Removing
    public static void main(String[] args) {

        BinaryNode <Integer> rootNode = new BinaryNode<> (new Integer(7), null);
        BinaryNode <Integer> NodeFour = new BinaryNode<> (new Integer(4), rootNode);
        BinaryNode <Integer> NodeTwelve = new BinaryNode<> (new Integer(12), rootNode);
        BinaryNode <Integer> NodeTwo = new BinaryNode<> (new Integer(2), NodeFour);
        BinaryNode <Integer> NodeSix = new BinaryNode <> (new Integer(6), NodeFour);
        BinaryNode <Integer> NodeNine = new BinaryNode <> (new Integer(9), NodeTwelve);
        BinaryNode <Integer> NodeNineteen = new BinaryNode <> (new Integer(19), NodeTwelve);
        
        rootNode.leftChild = NodeFour;
        rootNode.rightChild = NodeTwelve;

        NodeFour.leftChild = NodeTwo;
        NodeFour.rightChild = NodeSix;

        NodeTwelve.leftChild = NodeNine;
        NodeTwelve.rightChild = NodeNineteen;
     
        /** 
        System.out.println("----------- Adding New Node --------------");
        addNewBinaryNode(35, rootNode);

        System.out.println("---------------- Min ---------------------");
        searchMin(rootNode);

        System.out.println("---------------- Max ---------------------");
        searchMax(rootNode);
        */

        System.out.println("------------- In Order First------------");
        // inOrderTraversal(rootNode);
        traverseIteratively(rootNode);

        // System.out.println("---------------- Breadth First------------");
        // breadthFirstTraversal(rootNode);
    }
}

/**
 * Binary Tree
 * leftNode < parent < rightNode
 * 
 * There are 3 depth-first search method: 
 *  - in-order : Used to sort and search value inside tree
 *  - pre order : 
 *  - Post Order : Used when we want to access leafes first
 * -----------------------------------------------------------------
 * 
 * Breath First traverse means that we go through each note form the root tree.
 *  Then the next level down and down until we reach the maximul height of the tree.
 *  As we pass we need to keep checking nodes that are sharing the same depth.
 */


