package SimpleTree;

class TreeNode{
    int data;
    // Children Shoul Be A list
    TreeNode leftChild;
    TreeNode rightChild;

    TreeNode(int data){
        this.data = data;
        this.leftChild = null;
        this.rightChild = null;
    }

    static void traverse(TreeNode node){
        if (node == null){
            return;
        }else{
            System.out.println("yea " + node);
            traverse(node.leftChild);
            traverse(node.rightChild);
        }
    }

    static void inOrderTraverse(TreeNode root){

    }

    static void preOrderTraversal(TreeNode root){

    }

    static void postOrderTraversal(TreeNode root){
        
    }
    
    public String toString(){
        return "Value: " + this.data + "."; //+ ", leftChild -> " + this.leftChild; // ", rightChild: " + this.rightChild;
    }

    public static void main(String[] args) {
        TreeNode rootNode = new TreeNode(23);
        
        TreeNode leftNode = new TreeNode(10);
        rootNode.leftChild = leftNode;

        TreeNode rightNode = new TreeNode(13);
        rootNode.rightChild = rightNode;

        TreeNode sevenNode = new TreeNode(7);
        TreeNode nineNode = new TreeNode(9);
        leftNode.leftChild = sevenNode;
        rightNode.rightChild = nineNode;


        traverse(rootNode);
    }
}