import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array{

    public static void main(String[] args) {
      double [] a = new double[5];
      
      int [] b = {1,3,4,44};
      int [] arr1 = {6,9,13,15,20};
      int [] arr2 = {1,3,7,13,20};

      // numChars("aacccbbaa");
      // editAway("geeks", "geek");
      // editAway("geeks", "geeks");
      // editAway("peaks", "geeks");
      // editAway("geaks", "geeks");
      // compressString("aabbccaa");
      firstUniqueChars("cararicon".toLowerCase());

      // printReverse(arr2);
      
      /*
      System.out.println("Regular Print");
      printArray(b);

      System.out.println("Reverse Array");
      printReverse(b);

      System.out.println("Merging Two Arrays");
      System.out.println("que");
      mergeDoubleArray(arr1, arr2);

      System.out.println("Manipulating Strings");
      System.out.println(isPal("SSSS"));
      
      // HashCode
      for( Object obj : new Object[]{"a","A", new Integer(10),"a0", "a1", new Float(10.0)}){
        System.out.println( obj + " ==> " +  obj.hashCode());
      }
      

      // Replacing Spaces
      String wd1 = "Mr John Smith";
      String wd2 = "Was Here And All of the Crew";
      System.out.println(replaceSpaces(wd1));
      System.out.println(replaceSpaces(wd2));

      // Permutation
      System.out.println(isPermutation("ABCD", "DAbC"));
}

    static void printArray(int [] arr){
      for (int i: arr) {
        System.out.println(i);
      }
    }

    static void printReverse(int [] arr){
      int nums = arr.length;
      int result[] = new int [nums];
      
      for(int i = arr.length-1; i>=0; i--){
        result[(i+1) % nums ] = arr[i];
      }

      for (int num : result) {
        System.out.println(num);
      }
      */
    }

    static boolean isUnique(String word){
      boolean result = true;
      for(int currIdx = 0; currIdx < word.length(); currIdx++){
        for(int nxtdx = currIdx + 1; nxtdx < word.length(); nxtdx++){
          if(word.charAt(currIdx) == word.charAt(nxtdx)){
            result = false;
          }else{
            continue;
          }
        }
      }

      return result;
    }

    static HashMap<Character, Integer> uniqueNumChars(String word){
      HashMap<Character, Integer> uniqueMap = new HashMap<>();
      for (char letter : word.toCharArray()) {
        if(!uniqueMap.containsKey(letter)){
          uniqueMap.put(letter, 1);
        }
        else
        {
          Integer count =  uniqueMap.get(letter); //uniqueMap.containsKey(letter) ? uniqueMap.get(letter) : 1;
          uniqueMap.put(letter, count + 1);
        }
      }
      return uniqueMap;
    }

    static void numChars(String word){
      
      char currentLetter = word.charAt(0);
      int currentCount = 0; 
      String res = "";

      for(int currIdx = 0; currIdx<word.length(); currIdx++){   
         
        if(currentLetter == word.charAt(currIdx)){
          currentCount++;
          res=currentLetter + " "  +currentCount;
          System.out.println(currentLetter + " ==> " + currentCount);
        }
        else
        {
          currentLetter = word.charAt(currIdx);
          currentCount = 1;
          res=currentLetter + " "  +currentCount;
          System.out.println("New Letter " + word.charAt(currIdx) + " ==> " + currentCount);
        }
        
      }

      System.out.println(res);
    }

    static void editAway(String wd1, String wd2){
      
      int edit = 0;

      // Can Either Be Equal
      if(wd1.length() == wd2.length()){
        for(int i = 0; i < wd1.length(); i++){
          if(wd1.toLowerCase().charAt(i) != wd2.toLowerCase().charAt(i)){
            edit++;
          }
        }
      }

      // OR NOT
      String shorterString = wd1.length() < wd2.length() ? wd1.toLowerCase() : wd2.toLowerCase();
      String longerString = wd1.length() > wd2.length() ? wd1.toLowerCase() : wd2.toLowerCase();

      int shortIdx = 0;
      int longIdx = 0;

      int longerSize = longerString.length();
      int shorterSize  = shorterString.length();

      while(longIdx < longerSize && shorterSize < longerSize){

        if(shortIdx < shorterSize && shortIdx <= longIdx){
          if(shorterString.charAt(shortIdx) == longerString.charAt(longIdx)){
            shortIdx++;
            longIdx++;
          }else{
            edit++;
            longIdx++;
          }

        }else{
          edit++;
          longIdx++;
        }

      }

      System.out.println("Size " + wd1 + " v " + wd2 + " = " + edit);
    }

    static boolean isPal(String name){
      String isReversed = "";
      System.out.println("Checking " + name);
      for(int i = name.length()-1; i>=0; i--){
        isReversed += name.charAt(i);
      }
      return (isReversed.toUpperCase()).equals(name.toUpperCase());
    }

    static void mergeDoubleArray(int [] arr1, int [] arr2){

      int [] aux = new int[arr1.length+arr2.length];
      int auxIndex = 0;
      int leftArray = 0;
      int rightArray = 0;
      // && rightArray <= arr2.length
      while(leftArray<=arr1.length-1 || rightArray <= arr2.length-1 ){
        

        if( arr2[rightArray] <= arr1[leftArray]  ){
          System.out.println(arr2[rightArray]);
          rightArray++;
        }

        if( arr1[leftArray]<=arr2[rightArray]){
          System.out.println(arr1[leftArray]);
          leftArray++;
        }
       
      }

    }

    /*********************************************************************
     *                  String Replace %20
     *********************************************************************/

     static String replaceSpaces(String sentence){
       String [] splitWds = sentence.trim().split(" ");
       return String.join("%20", splitWds);
     }

    /*********************************************************************
     *                   2 Word Permutation
     *********************************************************************/   
    static boolean isPermutation(String wd1, String wd2){
      boolean res  = true;
      if(wd1.trim().length() != wd2.trim().length()){res = false;}

      HashMap <Integer, Character> wd1Map = new HashMap<>();
      for(int i =0; i<wd1.length(); i++){
        wd1Map.put(i, Character.toLowerCase(wd1.charAt(i)));
      }

      for(int i = 0; i<wd2.length(); i++){
        Character letter = Character.toLowerCase(wd2.charAt(i));
        if (wd1Map.containsValue(letter)){
          continue;
        }else{
          res = false;
          break;
        }
      }

      return res;
    }
     

    /*********************************************************************
     *                  String Substring
     *********************************************************************/     
    static void compressString(String word){
      String compressItem = word.toLowerCase().trim();
      String result = "";
      int count = 0;

      for(int i = 0; i < compressItem.length(); i++){
        while(i + 1 < compressItem.length()){
          System.out.println(i);
        }

      }
    }

    // static void foundChar(int start, int end, String word, char text){
    //   String pString = word.toLowerCase().trim();
    //   for(int i = start; i<end; i++){
    //     if(pString == pString.charAt(i)  ){
    //       System.out.println("Fount");
    //     }else{
    //       System.out.println("Not Found");
    //     }
    //   }
    // }

    static void firstUniqueChars(String word){
      HashMap <Character, Integer> res = new HashMap<Character, Integer>();
      for(int i = 0; i < word.toLowerCase().length(); i++){
        if(res.containsKey(word.toLowerCase().charAt(i))){
          res.computeIfPresent(word.toLowerCase().charAt(i), (k,v) -> v + 1);
        }else{
          res.put(word.toLowerCase().charAt(i), 1);
        }
      }

      for(int i = 0; i < word.toLowerCase().length(); i++ ){
        System.out.println(i);
      }
      
      System.out.println(Arrays.asList(res));
    }



}