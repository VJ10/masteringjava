public class Player implements Comparable<Player>{
    private int ranking;
    private String name;
    private int age;

    public Player(int ranking, String name, int age){
        this.ranking = ranking;
        this.name = name;
        this.age = age;
    }

    public void setRanking(int ranking){
        this.ranking = ranking;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setAge(int age){
        this.age = age;
    }

    public int getRanking(){
        return this.ranking;
    }

    public String getName(){
        return this.name;
    }

    public int getAge(){
        return this.age;
    }

    @Override
    /**
     * @param   anotherEmployee - The Employee to be compared.
     * @return  A negative integer, zero, or a positive integer as this employee
     *          is less than, equal to, or greater than the supplied employee object.
    */
    public int compareTo(Player o) {
        // Concise Way Of Writing It

        // if(this.getRanking() < o.getRanking()){
        //     return -1;
        // }else if(this.getRanking() > o.getRanking()){
        //     return 1;
        // }else{
        //     return 0;
        // }

        // return (this.getRanking() - o.getRanking()); //Short term
        return ( this.getName().compareTo(o.getName()) ); 
    }
}