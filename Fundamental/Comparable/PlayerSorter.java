import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PlayerSorter implements Comparator<Player>{
    public static void main(String[] args) {
        List <Player> footballTeam = new ArrayList<Player>();
        PlayerSorter sortAge = new PlayerSorter();

        
        Player p2 = new Player(2, "Morgan", 30);
        Player p3 = new Player(9, "Suarez", 32);
        Player p4 = new Player(3,  "Press", 31);
        Player p1 = new Player(1, "Messi", 32);

        footballTeam.addAll(Arrays.asList(p1,p2, p3, p4));
        Collections.sort(footballTeam);

        for (Player player : footballTeam) {
            System.out.println(player.getName());
        }
    }

    @Override
    public int compare(Player o1, Player o2) {
        return o1.getAge() - o2.getAge();
    }
}