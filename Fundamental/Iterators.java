import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Iterators {
    public static void main(String[] args) {
        List <String> items = new ArrayList<>();
        items.add("Messi");
        items.add("Carrie");
        items.add("Taylor");
        items.add("Selena");
        items.add(0, "Joe");
        items.addAll(Arrays.asList("Mary", "Caitlin","Kayla"));
        items.addAll(3, Arrays.asList("Mary", "Caitlin","Kayla"));
        Iterator <String> iterItemsIterator = items.iterator();

        while(iterItemsIterator.hasNext()){
           String nextValue = iterItemsIterator.next();
           if("Carrie".equals(nextValue)){
               iterItemsIterator.remove();
               System.out.println("Delete: " + nextValue);
           }
        }

        System.out.println("------------------------------------------------");
        System.out.println("List Iterator Interface");
        System.out.println("With The hasPrevious(), It starts from the end.");
        System.out.println("With The hasNext(), It starts from the beginning.");
        System.out.println("------------------------------------------------");
        ListIterator <String> listIterator = items.listIterator(items.size());

        while(listIterator.hasNext()){
            int nextIndex = listIterator.nextIndex();
            String nextWithIndex = items.get(nextIndex);
            
            String next = listIterator.next();
            System.out.println(next);

            if("Selena".equals(next)){
                listIterator.set("REPLACED");
            }
        }

        listIterator.add("SELENA");

        while(listIterator.hasPrevious()){
            int prevIdx = listIterator.previousIndex();
            String prevElement = items.get(prevIdx);
            
            String previous = listIterator.previous();  // Move To The Prev One
            System.out.println("Prev: " + previous) ;
        }

        System.out.println("------------------------------------------------");
        System.out.println("Final ListIterator");
        while(listIterator.hasNext()){
            String next = listIterator.next(); // Moves it and stores.
            System.out.println(next);
        }
        
        System.out.println("------------------------------------------------");


    }
}

/**
 * The Iterator Interface has three core methods. Iterator for collection dont guarantee iteration in any particular orrder unless an implementation provides it.
 * it allows us to modify a collection while traversing it, which is more difficult with a simple for/while statement. Gives us a good pattern we can use in many methods that only
 * required collection processing while maintaining good cohesion and low coupling. Can be used for any collection classes and it support both READ and REMOVE operation. However it support
 * only forward direction iteration that is Uni-Directional iterator. To overcome these limitation, Java introduced two more cursor: listIterator and SplitIterator
 * 
 * hasNext() used to check if there at least one element left to iterato over. its designed to be used as a condition in a while loop.
 * next() can be used for stepping over the next element and obtaining it.
 * remove() if we want to remove the current element from the collection, we can use the remove. This is the safe way to remove elements while iteration
 *          without the rist of ConcurrentModificationException.
 * 
 * Also Have ListIterator Interface which add new functionality for iterating over lists. Which works in bothe direction meaning that it works in the forward direction as well as
 * backward direction.
 * Can Perform Create & Update operations. Along with READ AND DELETE like the (Iterator Interface)
 * Forward Direction Iteratrion Method
 *  1. hasNext()
 *  2. next()
 *  3. nextIndex()
 * Backward Direction Iteration Method
 *  1. hasPrevious()
 *  2. previous()
 *  3. previousIndex()
 * add() => we add new element before the item that would be returned by next() and after tje one returned py previous
 * set() => lets us replace the element that was returned in the call to next() or previous()
 * However it not applicable for the whole Collection API and its not a universal Java Cursor.
 */