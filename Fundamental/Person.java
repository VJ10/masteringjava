import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Person implements Comparable<Person> {
    String name;

    Person(String name){
        this.name = name;
    }

    @Override
    public int compareTo(Person other){
        return this.name.compareTo(other.name);
    }
}

class SimpleSorting{
    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>();

        persons.add(new Person("Taylor"));
        persons.add(new Person("Jones"));
        persons.add(new Person("Howard"));
        persons.add(new Person("Casey"));

        // Collections.sort(persons);

        // persons.stream().map(s -> s.name).forEach(System.out::print);

        Collections.reverse(persons);
        persons.stream().forEach(System.out::print);

    }
}

/**
 * We often have to sort elements from database into collection, array or map. We can implement whatever sorting algorithms we want with any type.
 * Using Comparable interface and the compareTo() method. We can sort using alphabetical/reverse order.
 * 
 * The CompareTo() method compares given object with a specified object to determine order of objets. 
 *   Return Value        Then
 *     >= 1             then this.name > person.name
 *      0               then this.name = person.name
 *     <=1              then this.name < person.name
 * 
 * The comparator is more flexible. Allows us to do the same but in a more flexible way.
 */