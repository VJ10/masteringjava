# Java
Java Platform is a collection of programs that help programmers to develop and run Java programming applications efficiently. It includes an execution engine, a compiler and a set of libraries in it. 

## Features
- it's platform independent. Some programs developed in one machine can be executed in another machine. 
- its a multithreaded language with automatic memory management.
- its created for the distributed environment of the internet.

### Component of Language
Java is a high level language, thus the CPU never understand the source code. These chips only understand machine code. These machine codes run at the CPU level therefore it would be different machines codes for other models of CPU. The machine understand this source code and then translates them into machine understandable code, which is an executable code. All of these functionalities happens in the following platform.

1. Java Development Kit (JDK)
    Software development environment used to make Java applications. Java runs on all major OS. The JDK helps them code and run java programs. it's possible to install more than one jdk on the same computer.
    It contains tools required to write Java programs and JRE (runtime) to execute them. It includes a compiler (convert code written in Java into byte code), Java application launcher, Appletviewer. Java application launcher opens a JRE, loads the necessary class and executes its main method.
2. Java Virtual Machine (JVM)
   Engine that provides a runtime environment to drive the Java Code or applications. It convert the Java byte code into machine language. JVM is part of Java Run Environment (JRE). It has numerous libraries, tools and frameworks. It comes with JIT (Just In time) compiler that converts java source code into low level machine language. Hence, it runs faster than regular application.
   ![Jvm diagram](./diagram/JVM.png/)
3. Java Runtime Environment (JRE)
    Software designed to run other software. Can contains the class libraries, loader class and JVM. If you want to run a Java program then you need JRE. If you aren't a programmer then you don't need to install JDK. but just JRE to run program.


#### Different Types Java Platforms
1. Java SE (Standard Edition)
   offer Java core functionality. It defines all basis type and object to high level classes. It's used for networking, security, db access, gui development and XML parsing.
2. Java EE (Enterprise Edition)
   offers an API and runtime environment for developing and running highly scalable, large scale, multi-tiered, reliable and secure network applications.
3. Java ME (Micro Edition)
   offers small footprint virtual machine running Java programming languages application on small devices like mobile phones.
4. Java FX
   platform used to develop rich internet application using lightweight user interface API. it uses hardware accelerated graphics and media engines that help take advantage of higher performance client and modern look and feel and high level API for connecting networked data sources. 


#### How JVM works ?
1. Code display the addition of 2 numbers is : System.out.println(1+2) and saved as .java file.
2. Use Java compiler to convert into an intermediate code called byte code. The output is a .class file.
3. This code is only understood by the virtual platform called Java Virtual Machine.
4. This JVM resides in the RAM of the OS. When virtual machine is fed with this byte code, it identifies the platform its working with and converts the byte code into the native machine code.

#### How Java Platform Independent?
- Like the C compiler, the Java compiler does not produce native executable code for a particular machine. Instead, it produces a unique form of bytecode. It executes according to the rules laid out in virtual machine specification. Therefore, Java is a platform independent language.
- The Java source code can run any on all OS.


[More on Jvm!](https://www.guru99.com/java-virtual-machine-jvm.html)