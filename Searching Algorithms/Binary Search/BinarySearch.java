import java.util.Arrays;

public class BinarySearch{

    static int iterateSearch(int [] arr, int val){
        int low = 0 ; 
        int right = arr.length-1;
        
        
        while(low<=right){
            int mid = (low + right) / 2 ;

            if (val < arr[mid] ){
                right = mid-1;
            }
            if( val > arr[mid]){
                low = mid+1;
            }
            if(val == arr[mid]){
                return mid;
            }


        }
        return -1;
    }

    static int recursiveSearch(int [] arr, int val, int low, int high){
        int mid = (low + high) / 2; 

        // Base Case
        if (val == arr[mid]){
            return mid;
        } 
        
        if (val > arr[mid]) {
            // low = mid+1;
            return recursiveSearch(arr, val,  mid+1, high);
        } 
        
        if (val < arr[mid] && mid > 1) {
            // high = mid-1;
            return recursiveSearch(arr, val,  low, mid-1);
        } 

       return -1;
        
    }

    public static void main(String[] args) {
        int [] vals = {5,6,7,8,10,12,13,24,85,844,5484,8484,89987,84986};
        int [] val2 = {32,34,56,76,87};

        System.out.println("Starting Binary Search");
        System.out.println("\n");

        System.out.println("Iteration Method");
        System.out.println(BinarySearch.iterateSearch(vals, 13));
        System.out.println("\n");

        System.out.println("Recursive Method");
        // System.out.println( BinarySearch.recursiveSearch(vals, 13, 0, vals.length-1) );
        System.out.println( BinarySearch.recursiveSearch(val2, 34, 0, val2.length-1) );


        System.out.println("Java Built In: ");
        int [] nums = {2,3,54,657,767,3345};
        System.out.println("Index of 54 is " + Arrays.binarySearch(nums,0, nums.length - 1, 54));
    }
}

/**
 * Binary or Logarithmic Search one of the most commonly used search algorithms due to quick search time
 * Use the Divide and conquer method into equal halves 
 *      w/ each iteration compares the goal element with the  middle element
 *      if element is found the search ends
 *      else continue by dividing array in half
 * 
 * Binary is fster than linear search except for small arrays. But arrays must be sorted first 
 * Hash Tables      
 * Can Be Implemented in two ways
 *      iterative
 *      recursive 
 * Shouldnt be a big difference but varies based on the programming language.
 * 
 * Worst Complexity O(log n)
 * Best Complexity O(1)
 * 
 * Java Collection Class provides the binary search for List
 */