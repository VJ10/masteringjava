class LinearSearch{

    static int search(int [] arr, int num){
        int index = -1;
        for(int i=0; i<arr.length-1; i++){
           if (arr[i] == num) return
                index = i;
            // System.out.println("From Search Method: " + i); 
        }
        return index;
    }
    public static void main(String[] args) {
        int [] nums = {423,234,543,123,45,12,12,13,34};
        int valid = (search(new int []{43,53,10,54,56,76,9,11}, 10));
        search(nums, 12);
        System.out.println(search(nums, 12));
        System.out.println(valid);
    }
}

/**
 * Simplest Algorithm, All we have to do is loop through the array sequentially
 *  if its found return the index
 *  else return -1
 * They are really basic, shouldnt really use them in production.
 */