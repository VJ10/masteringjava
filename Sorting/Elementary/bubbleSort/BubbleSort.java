public class BubbleSort{

    static void sort(int [] arr){
        boolean sorted = false;

        while(!sorted){
            sorted = true;

            for (int i = 0; i<arr.length - 1; i++){
                if(arr[i + 1] < arr[i]){
                    swap(arr, i+1, i);
                    sorted = false;
                }
            }
        }
    }

    static void swap(int [] a, int i, int j){
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    static void printResult(int [] result){
        for(int item : result){
            System.out.print(item + " ");
        }
    }
    public static void main(String[] args) {
        int [] nums = {4, 2, 1, 5, 3};

        BubbleSort.sort(nums);

        printResult(nums);
    }
}