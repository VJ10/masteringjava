import java.util.Comparator;

public class Insertion {

    public static void main(String[] args) {
        Comparable [] a =  {565,548,87,11,48,98,774,854,15,1,5,3};
        Insertion.sort(a);
    }

    public static void sort(Comparable [] a)
    {
        
        for (int currentIndex = 0; currentIndex<a.length-1; currentIndex++){
            int nextIndex = currentIndex+1;

            // if the next number is less than the current number switch
            if(less(a[nextIndex],a[currentIndex])){
                exch(a, currentIndex, nextIndex);
                
                // we know we cant go back if we reach the first index so
                // until we get to it go back to the previous.
                while(currentIndex!=0 ){
                    if(less(a[currentIndex], a[currentIndex-1])){
                        exch(a, currentIndex, currentIndex-1);
                    }
                    currentIndex--;
                }

            }

        }
        Insertion.show(a);
        System.out.println(Insertion.isSorted(a));
    }


   /***************************************************************************
    *  Helper sorting functions.
    ***************************************************************************/
    
    // is v < w ?
    private static boolean less(Comparable v, Comparable w) {
        return v.compareTo(w) < 0;
    }

    // is v < w ?
    private static boolean less(Object v, Object w, Comparator comparator) {
        return comparator.compare(v, w) < 0;
    }

    // is v > w ?
    private static boolean greater(Comparable v, Comparable w) {
        return v.compareTo(w) > 0;
    }

    // is v > w ?
    private static boolean greater(Object v, Object w, Comparator comparator) {
        return comparator.compare(v, w) > 0;
    }
        
    // exchange a[i] and a[j]
    private static void exch(Object[] a, int i, int j) {
        Object swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

    // exchange a[i] and a[j]  (for indirect sort)
    private static void exch(int[] a, int i, int j) {
        int swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

   /***************************************************************************
    *  Check if array is sorted - useful for debugging.
    ***************************************************************************/
    private static boolean isSorted(Comparable[] a) {
        return isSorted(a, 0, a.length);
    }

    // is the array a[lo..hi) sorted
    private static boolean isSorted(Comparable[] a, int lo, int hi) {
        for (int i = lo + 1; i < hi; i++)
            if (less(a[i], a[i-1])) return false;
        return true;
    }

    private static boolean isSorted(Object[] a, Comparator comparator) {
        return isSorted(a, 0, a.length, comparator);
    }

    // is the array a[lo..hi) sorted
    private static boolean isSorted(Object[] a, int lo, int hi, Comparator comparator) {
        for (int i = lo + 1; i < hi; i++)
            if (less(a[i], a[i-1], comparator)) return false;
        return true;
    }

   // print array to standard output
    private static void show(Comparable[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }
}