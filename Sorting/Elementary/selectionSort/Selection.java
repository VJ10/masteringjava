import java.util.Comparator;
public class Selection{

    public static void main(String[] args) {
        Comparable [] a = {14,34,2,10};
        selectionSort(a);

        // int [] b = {14,11,6,19};
        // System.out.println("The Current Minimun Is " + getMinValue(b));
        
    }


    public static int getMinValue(int [] numbers){
        int minValue = numbers[0];

        for (int i=0; i<numbers.length; i++){
            if(numbers[i]<minValue){
                minValue = numbers[i];
            }

        }
        return minValue;
    }

    public static void selectionSort(Comparable[]a){
        for(int currentNumber = 0; currentNumber<a.length; currentNumber++){

            // Comparable minValue = a[currentNumber];
            int minValue = currentNumber;
            
            System.out.println("---------Start-------------");

            for(int nextNumber=currentNumber+1; nextNumber<a.length; nextNumber++){
                if(less(a[nextNumber],a[minValue])){
                    minValue = nextNumber;
                }

            }
            
            // System.out.println(minValue);
            System.out.println("Need to swap " + currentNumber);
            System.out.println("W/ " + minValue);

            Selection.exch(a, currentNumber, minValue);

            // System.out.println("New min value " +minValue);
            // System.out.println("Swappin Time.");
            // System.out.println("Next Number " + a[nextNumber]);
            // System.out.println("Summary : Min - " + minValue);
            
            System.out.println("---------END-------------");
            // System.out.print(a[currentNumber].toString());

        }

        Selection.show(a);
    }

                // if(less(a[i],minValue)){
            //     minValue=a[i];
            //     System.out.print("Current Min Changed : " + i);
            // } 

           // exch(Comparable [] a, int i, int j)

    /**
     * Rearranges the array in ascending order, using the natural order.
     * @param a the array to be sorted
    */
    public static void Sort(Comparable[]a){
        int n = a.length;
        for (int i = 0; i<n; i++){
            int min = i;
            for(int j=i+1; j<n;j++){
                System.out.println(j);
            }


        }
    }

    /******************************************************************************
    |*                     Helper Functions                                        |
    |*                                                                             |
     ******************************************************************************/


   /***************************************************************************
    *  Helper sorting functions.
    ***************************************************************************/
    
    // is v < w ?
    private static boolean less(Comparable v, Comparable w) {
        return v.compareTo(w) < 0;
    }

    // is v < w ?
    private static boolean less(Object v, Object w, Comparator comparator) {
        return comparator.compare(v, w) < 0;
    }
        
    // exchange a[i] and a[j]
    private static void exch(Object[] a, int i, int j) {
        Object swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

    // exchange a[i] and a[j]  (for indirect sort)
    private static void exch(int[] a, int i, int j) {
        int swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

   /***************************************************************************
    *  Check if array is sorted - useful for debugging.
    ***************************************************************************/
    private static boolean isSorted(Comparable[] a) {
        return isSorted(a, 0, a.length);
    }

    // is the array a[lo..hi) sorted
    private static boolean isSorted(Comparable[] a, int lo, int hi) {
        for (int i = lo + 1; i < hi; i++)
            if (less(a[i], a[i-1])) return false;
        return true;
    }

    private static boolean isSorted(Object[] a, Comparator comparator) {
        return isSorted(a, 0, a.length, comparator);
    }

    // is the array a[lo..hi) sorted
    private static boolean isSorted(Object[] a, int lo, int hi, Comparator comparator) {
        for (int i = lo + 1; i < hi; i++)
            if (less(a[i], a[i-1], comparator)) return false;
        return true;
    }

   // print array to standard output
    private static void show(Comparable[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }
}