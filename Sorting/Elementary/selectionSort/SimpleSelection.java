class SimpleSelection{
    int [] nums = new int [10];
    public SimpleSelection(int[] numbers){
        this.nums = numbers;
    }

    public void sort(){
        int currentMin = nums[0];
        int currIndex = 0;

        for(int i=0; i<nums.length-1; i++){
            if(currentMin > nums[i+1]){
                System.out.println("Swapped " + currentMin + "Vs." + nums[i]);
                currentMin = nums[i];
                currIndex = i;
                
            }else{
                System.out.println("All Good");
            }
            System.out.println("-------");
        }
    }



    public void swap(int num1Idx, int num2Idx, int [] nums){
        int temp = nums[num1Idx];
        nums[num1Idx] = nums[num2Idx];
        nums[num2Idx] = temp;
        System.out.println("Swapped");
    }
}